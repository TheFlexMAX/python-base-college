from models import Employee


def run():
    worker_1 = Employee(first_name='John', last_name='Snow', sex='male', grade_class=11, specialization='King',
                        position='Archmeister')
    worker_2 = Employee()
    worker_2.first_name = 'Leaf'
    worker_2.last_name = 'Sunny'
    worker_2.sex = 'female'
    worker_2.grade_class = 7
    worker_2.specialization = 'Shiny girl'
    worker_2.position = 'Grand Sun'

    worker_3 = Employee()
    worker_3.read()

    print(worker_1)
    print(worker_2)
    print(worker_3)


if __name__ == '__main__':
    run()
