from constants import PupilEnum
from models import Employee, Pupil, Person


def run():
    worker_1 = Employee(first_name='John', last_name='Snow', sex='male', grade_class=11, specialization='King',
                        position='Archmeister')
    worker_2 = Employee(first_name='Loana', last_name='Gray', sex='female', grade_class=9, specialization='Queen',
                        position='Librarian')
    worker_3 = Employee(first_name='Roman', last_name='Naumtsov', sex='male', grade_class=6, specialization='IT',
                        position='Joker')
    worker_1.append(worker_2)
    worker_1.append(worker_3)
    worker_1.show_colleagues()

    employee = Employee(first_name='Roman', last_name='Naumtsov', sex='male', grade_class=6, specialization='IT',
                        position='Joker')
    pupil = Pupil(first_name='Ann', last_name='Novoseltseva', sex='female', grade_class=8, specialization='Dog')
    person = Person(first_name='Bean', last_name='Pokedova', sex='female')

    print('*' * 8)

    worker_1.sort(PupilEnum.GRADE_CLASS.value)
    worker_1.show_colleagues()

    print('*' * 8)

    worker_1.pop()
    worker_1.show_colleagues()

    print('*' * 8)
    print(f'Size of: worker_1 is: {employee.__sizeof__()}')
    print(f'Size of: worker_2 is: {pupil.__sizeof__()}')
    print(f'Size of: worker_3 is: {person.__sizeof__()}')


if __name__ == '__main__':
    run()
