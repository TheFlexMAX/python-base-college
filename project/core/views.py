from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.views.generic import ListView
from django.views.generic.edit import FormMixin

from core.forms import TaskListForm, TaskForm
from core.models import TaskList, Task


class TaskListView(FormMixin, ListView):
    """
    Using for show and edit all task lists
    """
    model = TaskList
    template_name = 'task_list.html'
    http_method_names = ['get', 'post']

    # form mixin
    form_class = TaskListForm

    def get_queryset(self):
        return TaskList.objects.all()

    def get(self, request, *args, **kwargs):
        return self.render_content(request, self.form_class())

    def post(self, request, *args, **kwargs):
        self.form = self.form_class(request.POST)

        if self.form.is_valid():
            self.form.save()

        return self.render_content(request, self.form)

    def render_content(self, request, form):
        data = {
            'qs': self.get_queryset(),
            'form': form,
        }
        return render(request, self.template_name, data)


class TaskListDeleteView(View):
    """
    Using for delete some task list with contained tasks
    """
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        list_id = self.kwargs.get('id')

        task_list_obj = get_object_or_404(TaskList, id=list_id)
        task_list_obj.delete()

        return redirect('index')


class TaskView(FormMixin, ListView):
    """
    Using for add and show tasks from tasks list view
    """
    model = Task
    template_name = 'tasks.html'
    http_method_names = ['get', 'post']

    # form mixin
    form_class = TaskForm

    def get_queryset(self, *args, **kwargs):
        list_id = self.kwargs.get('id')
        task_list_obj = get_object_or_404(TaskList, id=list_id)
        return task_list_obj.tasks.all()

    def get(self, request, *args, **kwargs):
        list_id = self.kwargs.get('id')
        return self.render_content(request, self.form_class(initial={'task_list_id': list_id}))

    def post(self, request, *args, **kwargs):
        list_id = self.kwargs.get('id')
        self.form = self.form_class(request.POST)

        if self.form.is_valid() and list_id:
            Task.objects.create(
                name=request.POST.get('name'),
                task_list_id=list_id,
            )

        return self.render_content(request, self.form)

    def render_content(self, request, form):
        data = {
            'qs': self.get_queryset(),
            'form': form,
            'list_id': self.kwargs.get('id')
        }
        return render(request, self.template_name, data)


class TaskDeleteView(View):
    """
    Using for delete some task from some task list
    """
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        list_id = self.kwargs.get('id')
        task_id = self.kwargs.get('task_id')

        get_object_or_404(TaskList, id=list_id)
        task_obj = get_object_or_404(Task, id=task_id)
        task_obj.delete()

        return redirect('tasks', id=list_id)


class TaskCompleteView(View):
    """
    Using for set \ unset is_done status for task
    """
    def get(self, request, *args, **kwargs):
        list_id = self.kwargs.get('id')
        task_id = self.kwargs.get('task_id')

        get_object_or_404(TaskList, id=list_id)
        task_obj = get_object_or_404(Task, id=task_id)

        if task_obj.is_done:
            task_obj.is_done = False
        else:
            task_obj.is_done = True

        task_obj.save(update_fields=['is_done'])

        return redirect('tasks', id=list_id)

