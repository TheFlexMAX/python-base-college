from django.db import models


class CoreModel(models.Model):
    """
    Using for adding to all basic models
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ['id']


class TaskList(CoreModel):
    """
    Using for contain all tasks list
    """
    name = models.CharField(max_length=128)

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        return self.name


class Task(CoreModel):
    """
    Using for contains all tasks of user
    """
    name = models.CharField(max_length=128)
    is_done = models.BooleanField(default=False)
    task_list = models.ForeignKey(TaskList, related_name='tasks', on_delete=models.CASCADE)

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        return self.name
