from django.urls import path

from core.views import TaskListView, TaskView, TaskDeleteView, TaskListDeleteView, TaskCompleteView

urlpatterns = [
    path('', TaskListView.as_view(), name='index'),
    path('task-list/<int:id>/', TaskView.as_view(), name='tasks'),
    path('task-list/<int:id>/delete', TaskListDeleteView.as_view(), name='task_list-delete'),
    path('task-list/<int:id>/task/<int:task_id>/delete/', TaskDeleteView.as_view(), name='task-delete'),
    path('task-list/<int:id>/task/<int:task_id>/complete/', TaskCompleteView.as_view(), name='task-complete'),
]
